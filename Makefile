# System variables

imageDevelop=asciidoctor/docker-asciidoctor
dockerCommand=docker run
dockerCommand+= --rm --interactive --tty
dockerCommand+= --volume ${PWD}:/documents

# Targets

pdf-docker:
pdf: ## Create PDF by sources
	@mkdir -p build
	@$(MAKE) --silent sample.pdf

%.pdf:
	@echo "Create: build/$*.pdf"
	@asciidoctor-pdf \
	--require asciidoctor-diagram \
	--attribute pdf-style=.pdf-style.yml \
	--out-file "build/$*.pdf" \
	"src/$*.adoc"

prune-docker:
prune: ## Remove all artifacts of targets
	rm --recursive --force build

fix-owner-docker:
fix-owner: ## Fix owner for files
	chown 1000:1000 -R .

in-docker: ## Run command in docker, use: [opt=<docker-options>] cmd='<command>'
	${dockerCommand} ${opt} ${imageDevelop} ${cmd}


<target>-docker: ## Run any target in docker
%-docker:
	@$(MAKE) --silent in-docker opt=${docker-opt} cmd="make $* opt=${opt}"

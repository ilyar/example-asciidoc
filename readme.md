# Example Asciidoctor

## Requirement

```shell script
brew install make
brew install git
brew install docker
```

## Use

**Clone project:**
```shell script
git clone https://gitlab.com/ilyar/example-asciidoc.git
cd example-asciidoc
```

**Create PDF by AsciiDoc:**
```shell script
make pdf
```

**Open PDF:**
```shell script
open example-asciidoc.pdf
```
